var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

var fs = require("fs");

var clientsTxt = fs.readFileSync("messageGET.json");
var clientsJSON = JSON.parse(clientsTxt);

app.get('/clientes', function (req,res) {
  res.send (clientsJSON);
});

app.get('/clientes/:idcliente', function (req,res) {
  if (req.params.idcliente > 4 || req.params.idcliente < 1) {
    res.status(204);
    res.send('Client not found');
  }
  else {
    res.send (clientsJSON.clientes[req.params.idcliente-1]);
  }
});

app.post('/clientes', function (req,res) {
  res.sendFile (path.join(__dirname, 'messagePOST.json'));
});

app.put('/clientes', function (req,res) {
  res.sendFile (path.join(__dirname, 'messagePUT.json'));
});

app.delete('/clientes', function (req,res) {
  res.sendFile (path.join(__dirname, 'messageDELETE.json'));
});
